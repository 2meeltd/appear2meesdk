//
//  Appear2mee.h
//  Appear2mee
//
//  Created by Gerard Allan on 31/01/2019.
//  Copyright © 2019 2mee. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 Version V1.1.3, Appear2mee class. Provides static methods to connect to the 2mee Exchange, and to handle remote notifications from the 2mee Exchange.
 */


#import "Appear2meeContentView.h"
#import "FMView.h"


/* Appear2mee settings keys */

// Dialog/Content Extension Button text.
extern const NSString * _Nonnull kAppear2meeNotificationDismiss ;
extern const NSString * _Nonnull kAppear2meeNotificationAccept;
extern const NSString * _Nonnull kAppear2meeNotificationPlay;

extern const NSString * _Nonnull kAppear2meeWatchNotificationViewInApp;

extern const NSString * _Nonnull kAppear2meeMessageExpired;

// Position of face in app, key.
extern const NSString * _Nonnull kAppear2meeFaceHeadPosition;

// Possible face positions, values
extern const NSString * _Nonnull kAppear2meeFaceHeadTopRight;
extern const NSString * _Nonnull kAppear2meeFaceHeadTopLeft;
extern const NSString * _Nonnull kAppear2meeFaceHeadBottomRight;
extern const NSString * _Nonnull kAppear2meeFaceHeadBottomLeft;
extern const NSString * _Nonnull kAppear2meeFaceHeadCenter;

// Position of face with shoulders or torso in app, key
extern const NSString * _Nonnull kAppear2meeFaceShouldersPosition;

// Possible face with shoulders or torso positions in app, values
extern const NSString * _Nonnull kAppear2meeFaceShouldersRight;
extern const NSString * _Nonnull kAppear2meeFaceShouldersLeft;
extern const NSString * _Nonnull kAppear2meeFaceShouldersCenter;

// In app customisation
extern const NSString * _Nonnull kAppear2meeDisplayBackgroundColor;
extern const NSString * _Nonnull kAppear2meeFaceDisplayBackgroundColor;
extern const NSString * _Nonnull kAppear2meeFaceCenterBackgroundColor;

extern const NSString * _Nonnull kAppear2meeFaceAlertDismissTime;

// Notification sounds
extern const NSString * _Nonnull kAppear2meeNotificationSound;
extern const NSString * _Nonnull kAppear2meeFaceNotificationSound;

// Content Extension background images
extern const NSString * _Nonnull kAppear2meeContentExtensionBackgroundImage;
extern const NSString * _Nonnull kAppear2meeFaceContentExtensionBackgroundImage;

// Does exchange key __OpenExternalURL open the url value in a browser automatically.
extern const NSString * _Nonnull kOpenExternalURLAutomatically;

@class UNNotificationResponse; @class UNNotification;@class UNMutableNotificationContent;@class UNNotificationContent;@class UNNotificationCategory;@class UNNotificationContent;@class BGTask;


@interface Appear2mee : NSObject

/**
 *  Initialise the Appear2mee connection.
 *
 *  @param  appear2meeID ID supplied by the 2mee Exchange
 *  @param  appear2meeKey Key supplied by the 2mee Exchange
 *  @param  appgroup The common app group eg., group.com.2mee.Appear2mee, used to share files.
 */

+ (void)initWithID:(NSString* _Nonnull) appear2meeID  clientKey:(NSString* _Nonnull)appear2meeKey usingAppGroup: (NSString * _Nonnull) appgroup;


/**
 @name Communication with Exchange
 */

/**
 *  Registers the deviceToken to allow Appear2mee to receive remote notifications from the 2mee Exchange server.
 *  Should be used in the (UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken: call of your Application Delegate.
 *
 *  @param  deviceToken The deviceToken supplied in the didRegisterForRemoteNotificationsWithDeviceToken:
 *  @return The device 2meeID as an NSString.
 */
+ (NSString * _Nonnull)registerDeviceToken:(NSData* _Nonnull)deviceToken;


/**
 *  Registers the UserID. This is a custom user id the has meaning to developer. Typically it is a CRM identifer. This setting is optional, but when set 2mee Exchange reports make use of the userID
 *
 *  @param userid an NSString representing a (usually unique) custom identifier.
 */
+ (void)registerUserID:(NSString *_Nonnull) userid;

/**
 *  Registers the tags that the device subscribes to for remote notifications from the 2mee Exchange.
 *
 *  @param tags an NSDictionary of NSString
 */
+ (void)registerTags:(NSDictionary* _Nonnull)tags;

/**
 *  Returns the tags that have been registered with 2mee Exchange for remote notifications.
 *  A Dictionary of NSStrings.
 *
 *  @return A dictionary of the tags
 */
+ (NSDictionary* _Nonnull) tags;

/**
@name In App Message
*/

/**
 *      Attempts to get data associated with a message name. A Message name referers to a message (holocapsule) on the 2mee Exchange. On success callback with be called with a dictionary containing a message item data. If the message item does not exist, or the data, including the actual message files, cannot be downloaded, the callback  item will be null.
 *   The only use a message item can put to is to display it using the displayMessage: method.
 *      The callback can take a number of seconds to return as video files may need to be downloaded.
 *
 *  @param name an NSString of the slot name.
 *  @param callback a callback function that will provide a nulllable slotItem (NSDictionary *).
 */

+(void) getMessage:(NSString *_Nonnull) name callback:(nonnull void (^)(NSDictionary * _Nullable item)) callback;

/**
 * Presents a "dialog" depending on the slot message type. Calls the appropriate action function on user response.  Allows for text/image/animation/video/face slotItem message display. A slotItem is obtained from the getSlot: method.
 *
 * @param item  a message item (NSDictionary *).
 * @param acceptAction a void (^)(NSDictionary *) block to be executed if the "accept/OK" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values present in the slotItem, defined in the 2mee Exchange.
 * @param dismissAction a void (^)(NSDictionary *) block to be executed if the "Dismiss" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values present in the slotItem, defined in the 2mee Exchange.
 * @return success of display. Item may not exist.
 */

+(BOOL) displayMessage:(NSDictionary *_Nonnull) item withAcceptAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) acceptAction withDismissAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) dismissAction;



/**
 @name Notification handling in the App
 */

/**
 * Adds a UNNotificationCategory for the Appear2mee content extension actions to the existing set of UNUserNotificationCenter NotificationCategories.
 *
 * Should only be used in the UNUserNotificationCenter requestAuthorizationWithOptions:options
 completionHandler: method,
 *
 * Note add and other UINotificationCategory first as this method adds to the existing set. If you replace the set later then the Appear2mee  UNNotificationCategory will be lost.
 * Only use this method if you are providing the Appear2mee Notification Extension in your app.
 *
 */
+(void) addAppear2meeNotificationCategory;


/**
 Checks if a (UNNotification *)notification is a notification originating from the 2mee Exchange
 
 @param notification The notification
 @returns True, if a 2mee Exchange notification, else false.
 */
+ (BOOL) isAppear2meeNotification:(UNNotification *_Nullable)notification;

/**
 Checks if a (NSDictionary *) userInfo is a userInfo originating from the 2mee Exchange
 
 @param userInfo The dictionary to test.
 @returns True, if a 2mee Exchange notification, else false.
 */
+ (BOOL) isAppear2meeUserInfo:(NSDictionary *_Nullable)userInfo;


/**
 Returns KeyValues dictionary from an Appear2mee UNNotification notification . Return a keyValues dictionary containing data sent from the 2mee Exchange. If no values attached the dictionary is empty. If the UNNotification notification was not from an Appear2mee notification the returned value is nil.
 
 @param notification The notification
 @returns A dictionary of keyValues or nil if the response is not an Appear2mee notification
 */
+ (NSDictionary *_Nullable) notificationKeyValues:(UNNotification *)notification;


/**
 * Display a dialog using the contents of a UNNotification. Allows for text/image/animation/video/face message display.
 * Called only in the userNotificationCenter:willPresentNotification:withCompletionHandler: method
 *
 * @param notification A UNNotification received in the
 * @param acceptAction a void (^)(NSDictionary *) block to be executed if the "accept/OK" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined in the 2mee Exchange.
 * @param dismissAction a void (^)(NSDictionary *) block to be executed if the "Dismiss" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined in the 2mee Exchange.
 
 */
+(void) displayNotification:(UNNotification *_Nonnull)notification withAcceptAction:(void (^_Nonnull)(NSDictionary *_Nonnull)) acceptAction withDismissAction:(void (^_Nonnull)(NSDictionary *_Nonnull)) dismissAction;


/**
 * Handle a notification response. If response is from content extension then will complete the appropriate action function. If default responses will either call the dismiss action block or display a dialog using the contents of a UNNotificationResponse. Allows for text/image/animation/video/face message display.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method. 
 * @param acceptAction a void (^)(NSDictionary *) block to be executed if the "accept/OK" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined in the 2mee Exchange.
 * @param dismissAction a void (^)(NSDictionary *) block to be executed if the "Dismiss" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined in the 2mee Exchange.
 */
+(BOOL) didReceiveNotificationResponse:(UNNotificationResponse *_Nonnull)response withAcceptAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) acceptAction withDismissAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) dismissAction;

/**
 *  Returns KeyValues dictionary from an Appear2mee UNNotificationResponse response . Return a keyValues dictionary containing data sent from the 2mee Exchange. If no values attached the dictionary is empty. If the UNNotificationResponse response was not from an Appear2mee notification the returned value is nil.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
 *    @return A dictionary of keyValues or nil if the response is not an Appear2mee notification
 */
+(NSDictionary *_Nullable) notificationResponseKeyValues:(UNNotificationResponse *_Nonnull)response;

/**
 *  Returns true if the UNNotificationResponse response is from an Appear2mee notification else false.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
 *    @return true if an Appear2mee notification else false.
 */
+(BOOL) isAppear2meeNotificationResponse:(UNNotificationResponse *_Nonnull)response;

/**
 *  Returns KeyValues dictionary from an Appear2mee UNNotificationResponse response . Return a keyValues dictionary containing data sent from the 2mee Exchange. If no values attached the dictionary is empty. If the UNNotificationResponse response was not from an Appear2mee notification the returned value is nil.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
 *    @return A dictionary of keyValues or nil if the response is not an Appear2mee notification
 */
+(NSDictionary *_Nullable) notificationResponseKeyValues:(UNNotificationResponse *_Nonnull)response;

/**
 *  Returns true if the UNNotificationResponse response is from an Appear2mee notification else false.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
 *    @return true if an Appear2mee notification else false.
 */
+(BOOL) isAppear2meeNotificationResponse:(UNNotificationResponse *_Nonnull)response;



/**
 *  Forwards the push notification to Appear2mee. If the notification payload is not recognised as a 2mee message the function returns false, leaving the app to deal with it. A 2mee message is processed and the media is downloaded. Currently the media can be stored awaiting another push alert.
 
 This function should be called in the application delegate application:didReceiveRemoteNotification:fetchCompletionHandler: method.
 
 Important: The application must have the appropriate setting in its Capablities to allow background fetch push notifications.
 
 @param payload The payload dictionary from the application:didReceiveRemoteNotification:fetchCompletionHandler: method args.
 @param completionHandler The callback block from the application:didReceiveRemoteNotification:fetchCompletionHandler: method args.
 */
+(BOOL) didReceiveRemoteNotification:(NSDictionary * _Nonnull) payload fetchCompletionHandler:(void (^_Nullable)(UIBackgroundFetchResult result))completionHandler;

/**
 @name Permissions
 */

/**
 Permission to download FM. See also <WifiOnlyPermission:>.
 
 Note this permission will block downloads of Face Messages completely. If you need to block only WWAN (3G/4G) then download permission is required, and the  <WifiOnlyPermission:> should be set.
 
 
 @param  allowDownload set permission to download Face messages video. Default is true, face messages can be downloaded, but see also the <WifiOnlyPermission:> which my restrict downloads.
 */
+ (void) downloadPermission: (BOOL) allowDownload;

/**
 *  Returns the permission state for downloads of FM. See also <downloadPermission:> and <WifiOnlyPermission:>
 *
 *  @return true if permission to allow downloads is set.
 */
+ (BOOL) hasDownloadPermission;

/**
 Permission To Download FM Via Wifi network only. See also <downloadPermission:>.
 
 Note, if Wifi is not available only the fallback text of a face message will be delivered as an alert.
 
 @param onlyWifi set permission to permit only Wifi downloads of Face messages video. Subject to the permission to download messages given by <hasDownloadPermission>
 
 Default is false, meaning that any method of download is allowed, subject to <hasDownloadPermission>. If true then only Wifi can be used to download a message.
 */
+ (void) wifiOnlyPermission: (BOOL) onlyWifi;

/**
 *  Permission To Download files only via Wifi. See also <WifiOnlyPermission:>.
 *
 *  @return true if permission to use Wifi only is set.
 */
+ (BOOL) hasWifiOnlyPermission;

/**
 Permission for Message service.
 
 Note this permission will send a message to the 2mee Exchange to stop any new mesages from arriving. Alerts that are in transit or queued will still arrive and will be displayed.
 
 @param  allowService set permission to allow Face messages to be sent. Default is true.
 */
+ (void) servicePermission: (BOOL) allowService;

/**
 *  Permission for 2mee exchange service. See also <servicePermission:>.
 *
 *  @return true if permission to use service has been set.
 */
+ (BOOL) hasServicePermission;

/**
 *  Permission for sound to play when mute switch is on (default is play sound).
 *
 *  @return True, if permission to play sound is set.
 */
+ (BOOL) hasSoundOnMutePermission;


/**
 * Set Permission for sound to play when mute switch is on .
 *
 * @param  allowSound Set permission to allow sound when mute is on. Default is true.
 */
+ (void) soundOnMutePermission: (BOOL) allowSound;

/**
 @name Customisation Settings
 */

/**
 * Sets the default setting using an NSDictionary for settings. This dictionary will be merged with the current settings used by Appear2mee.
 * key values can include
 * kAppear2meeNotificationDismiss, kAppear2meeNotificationAccept, kAppear2meeNotificationPlay
 * Position of face in app.
 * kAppear2meeFaceHeadPosition, kAppear2meeFaceShouldersPosition,
 * Colors.
 * kAppear2meeDialogBackgroundColor, kAppear2meeFaceCenterBackgroundColor
 *
 *  @param defaultSettingDict NSDictionary of default settings to use.
 */
+(void) defaultSettings:(NSDictionary *_Nonnull) defaultSettingDict;

/**
 * Helper function to convert a UIColor to an NSNumber to allow a color to be stored in the Appear2mee setting dictionary. .
 *
 * @param color The UIClor to be converted into a NSNumber representation.
 * @return An NSNumber that represents the UIColor.
 */
+(NSNumber *_Nonnull) rgba:(UIColor *_Nonnull) color;




/**
 @name Test Notification
 */

/**
 * Perform a test local notification.
 *
 * It for testing purposes only
 *
 * @param payload The NSDictionary that matches an Appear2mee notification payload.
 */
+(void) testNotification:(NSDictionary *_Nonnull) payload;
/**
 @name Service Extension
 */

/**
 A method to be used in the Service Extension. Used to handle the download of files. Returns true if this is a Appear2mee notification, false if the notification is another type which should be handled by the developer.
 The function will modified the title/subtitle and body of the content paramater before attempting to download the files, so the bestAttempt will have the correct text if there is a failure.
 If files are downloaded then this method handles all the attacment creation and calls the contentHandler handler.
 
 @param content The mutable notification content which is the default bestAttempt if this function cannot download the files in time.
 @param requestIdentifier The identifier of the UNNotificationRequest  processed by the Service Extension didReceiveNotificationRequest: method.
 @param contentHandler The void (^)(UNNotificationContent * _Nonnull) of the Service Extension didReceiveNotificationRequest: method.
  *  @param  appgroup The common app group eg., group.com.2mee.Appear2mee, used to share files.
 
 */
+(BOOL) handleAppear2meeNotificationContent:(UNMutableNotificationContent *_Nonnull) content request:(NSString *_Nonnull)requestIdentifier withContentHandler:(void (^_Nonnull)(UNNotificationContent * _Nonnull)) contentHandler usingAppGroup:(NSString *_Nonnull) appgroup;


/**
 Used to cancel the download of files if the serviceExtensionTimeWillExpire method is called in a Service Extension.
 
 @param requestIdentifier The identifier of the UNNotificationRequest  processed by the Service Extension didReceiveNotificationRequest: method.
 
 */
+(void) cancelDownloadForRequest:(NSString *_Nonnull)requestIdentifier;


+(void) setPrivateAPI:(BOOL) privateApi;
@end


