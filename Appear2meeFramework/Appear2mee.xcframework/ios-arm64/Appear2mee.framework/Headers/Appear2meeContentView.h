//
//  Appear2meeContentView.h
//  Appear2meeSimple
//
//  Created by Gerard Allan on 01/02/2019.
//  Copyright © 2019 2Mee Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
@class UNNotification;
@class UNNotificationResponse;
NS_ASSUME_NONNULL_BEGIN

/** A UIView to display 2mee messages in a Content Extension.
 */

@interface Appear2meeContentView : UIView
/**
 *  Display the contents of a 2mee Exchange UINotification in a content extension
 *
 *  @param  notification The content extension UNNotification to display.
 *  @param  controller The NotificationViewController which conforms to the UNNotificationContentExtension protocol.
 *  @param  appgroup The common app group eg., group.com.2mee.Appear2mee, used to share files.
 */

-(void) displayNotification:(UNNotification *) notification notificationController: (UIViewController<UNNotificationContentExtension> *) controller  usingAppGroup:(NSString *) appgroup;

/**
 *  Processes the responses sent from the UNNotificationContentExtension, notification button presses.
 *
 *  @param response The UNNotificationResponse sent from UNNotificationContentExtension actions.
 *  @param completion The completion method supplied by the UNNotificationContentExtension
 */
-(void) didReceiveNotificationResponse:(UNNotificationResponse *)response completionHandler:(void (^)(UNNotificationContentExtensionResponseOption))completion;

@end

NS_ASSUME_NONNULL_END
