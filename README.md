# Appear2meeSDK

## Installation

Appear2meeSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Appear2meeSDK'
```

## Author

2mee Ltd, Gerard.Allan@2mee.com

## License

Appear2meeSDK is available under the 2mee Ltd license. See the LICENSE file for more info.
